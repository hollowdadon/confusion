import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType = ContactType;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required': "First name is required",
      'minlength': "First name must be at least 2 charechters a long",
      'maxlength': "First name cannot be more then 25 symbols"
    },
    'lastname': {
      'required': "Last name is required",
      'minlength': "Last name must be at least 2 charechters a long",
      'maxlength': "Last name cannot be more then 25 symbols"
    },
    'telnum': {
      'required': "Telnum is required",
      'pattern': "Must consist of only with numbers",
    },
    'email': {
      'required': "Email is required",
      'email': "Email in not valid format,example@gmail.com",
    }
  };

  constructor(private fb: FormBuilder) { 
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ] ],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ] ],
      telnum: ['', [Validators.required, Validators.pattern ] ],
      email: ['', [Validators.required, Validators.email ] ],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }

  onValueChanged(data?: any) {
    if(!this.feedbackForm) { return;}
    const form = this.feedbackForm

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: '',
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
  }

}
